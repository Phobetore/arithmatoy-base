# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    pass


def S(n: str) -> str:
    pass


def addition(a: str, b: str) -> str:
    pass


def multiplication(a: str, b: str) -> str:
    pass


def facto_ite(n: int) -> int:
    pass

# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return "S" * n + "0"


def S(n: str) -> str:
    return "S" + n


def addition(a, b) -> str:
    a_str = nombre_entier(a) if isinstance(a, int) else a
    b_str = nombre_entier(b) if isinstance(b, int) else b

    count = a_str.count("S") + b_str.count("S")
    return nombre_entier(count)


def multiplication(a: str, b: str) -> str:
    a_str = nombre_entier(a) if isinstance(a, int) else a
    b_str = nombre_entier(b) if isinstance(b, int) else b

    if a_str == "0" or b_str == "0":
        return "0"
    result = "0"
    for _ in range(len(a_str) - 1):
        result = addition(result, b_str)
    return result


def facto_ite(n: int) -> int:
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


def facto_rec(n: int) -> int:
    return 1 if n == 0 else n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    a, b = 0, 1
    for _ in range(n):
        a, b = b, a + b
    return a


def golden_phi(n):
    a = 1
    b = 1
    for _ in range(n):
        a, b = b, a + b
    return b / a


def sqrt5(n):
    phi = golden_phi(n+1)
    return 2 * phi - 1


def pow(a: float, n: int) -> float:
    return a ** n

def facto_rec(n: int) -> int:
    pass


def fibo_rec(n: int) -> int:
    pass


def fibo_ite(n: int) -> int:
    pass


def golden_phi(n: int) -> int:
    pass


def sqrt5(n: int) -> int:
    pass


def pow(a: float, n: int) -> float:
    pass
